# set_hostname

This role sets the name of the host based on set_hostname_hostname variable, if
defined. Otherwise, inventory_hostname will be used.

# Example Playbook

```yaml
---
- name: Set hostname
  hosts: "all"
  roles:
    - role: set_hostname
```

# License

GPLv3

# Author information

Wallun <wallun AT disroot DOT org>
